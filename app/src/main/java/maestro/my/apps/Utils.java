package maestro.my.apps;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.TypedValue;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Artyom on 17.01.2016.
 */
public class Utils {

    public static final String TAG = Utils.class.getSimpleName();

    public static final String PREF_SORT = "sort";
    public static final String PREF_VIEW_MODE = "view_mode";

    private static SimpleDateFormat mDateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm");

    public static List<AppInfo> getApps(Context context, ListOption option, ListSort sort) {
        final PackageManager pm = context.getPackageManager();
        final List<ApplicationInfo> allApps = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        final List<AppInfo> outApps = new ArrayList<>();

        for (ApplicationInfo appInfo : allApps) {
            final boolean isSystemApp = (appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            final boolean isCanBeMoved = (appInfo.flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) != 0;
            boolean shouldBeAdded = false;
            if (option != ListOption.SystemAppsOnly && !isSystemApp) {
                shouldBeAdded = true;
            } else if (isSystemApp && option == ListOption.SystemAppsOnly || option == ListOption.WithSystemApps) {
                shouldBeAdded = true;
            }
            //TODO: add movement ability and check
            if (shouldBeAdded) {
                try {
                    outApps.add(new AppInfo(context, appInfo, context.getPackageManager().getPackageInfo(appInfo.packageName, PackageManager.GET_META_DATA)));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        Collections.sort(outApps, new Comparator<AppInfo>() {
            @Override
            public int compare(AppInfo lhs, AppInfo rhs) {
                return lhs.getTitle().compareTo(rhs.getTitle());
            }
        });
        if (sort == ListSort.Size) {
            Collections.sort(outApps, new Comparator<AppInfo>() {
                @Override
                public int compare(AppInfo lhs, AppInfo rhs) {
                    return lhs.getSize() > rhs.getSize() ? 1 : -1;
                }
            });
        } else if (sort == ListSort.Date) {
            Collections.sort(outApps, new Comparator<AppInfo>() {
                @Override
                public int compare(AppInfo lhs, AppInfo rhs) {
                    return lhs.getDate() > rhs.getDate() ? 1 : -1;
                }
            });
        }

        //TODO: add sort option

        return outApps;
    }

    public static List<AppInfo> searchApps(Context context, String query) {
        final PackageManager pm = context.getPackageManager();
        final List<ApplicationInfo> allApps = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        final List<AppInfo> primaryMatchApps = new ArrayList<>();
        final List<AppInfo> secondaryMatchApps = new ArrayList<>();
        final String fixedQuery = query.toLowerCase().trim();

        for (ApplicationInfo appInfo : allApps) {
            String label = String.valueOf(appInfo.loadLabel(context.getPackageManager())).toLowerCase();
            if (label.startsWith(fixedQuery)) {
                try {
                    primaryMatchApps.add(new AppInfo(context, appInfo, context.getPackageManager().getPackageInfo(appInfo.packageName, PackageManager.GET_META_DATA)));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (label.contains(fixedQuery)) {
                try {
                    secondaryMatchApps.add(new AppInfo(context, appInfo, context.getPackageManager().getPackageInfo(appInfo.packageName, PackageManager.GET_META_DATA)));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        ArrayList outApps = new ArrayList();
        outApps.addAll(primaryMatchApps);
        Collections.sort(outApps, new Comparator<AppInfo>() {
                    @Override
                    public int compare(AppInfo lhs, AppInfo rhs) {
                        return lhs.getTitle().compareTo(rhs.getTitle());
                    }
                }
        );
        if (outApps.size() < 3) {
            Collections.sort(secondaryMatchApps, new Comparator<AppInfo>() {
                @Override
                public int compare(AppInfo lhs, AppInfo rhs) {
                    return lhs.getTitle().compareTo(rhs.getTitle());
                }
            });
            outApps.addAll(secondaryMatchApps);
        }
        return outApps;
    }

    public static List<RunningTask> getRunningTasks(Context context) {
        final HashMap<String, RunningTask> mTasks = new HashMap<>();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager.getRunningAppProcesses();

        String packageName;

        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            try {
                packageName = info.processName;
                RunningTask task = mTasks.get(packageName);
                if (task == null) {
                    ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    task = new RunningTask(context, appInfo, packageInfo);
                    mTasks.put(packageName, task);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        List<ActivityManager.RunningTaskInfo> runningTasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo info : runningTasks) {
            try {
                packageName = info.baseActivity.getPackageName();
                RunningTask task = mTasks.get(packageName);
                if (task == null) {
                    ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    task = new RunningTask(context, appInfo, packageInfo);
                    mTasks.put(packageName, task);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        List<ActivityManager.RunningServiceInfo> serviceInfos = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo info : serviceInfos) {
            if ((!info.started && info.clientLabel == 0)
                    || (info.flags & ActivityManager.RunningServiceInfo.FLAG_PERSISTENT_PROCESS) != 0) {
                continue;
            }
            try {
                packageName = info.process;
                RunningTask task = mTasks.get(packageName);
                if (task == null) {
                    ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    task = new RunningTask(context, appInfo, packageInfo);
                    mTasks.put(packageName, task);
                }
                task.addServiceInfo(info);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>(mTasks.values());
    }

    public static String formatDate(long date) {
        synchronized (mDateFormatter) {
            return mDateFormatter.format(new Date(date));
        }
    }

    public static String formatSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static ListSort getSort(Context context) {
        return ListSort.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_SORT, ListSort.Alphabet.name()));
    }

    public static void setSort(Context context, ListSort sort) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(PREF_SORT, sort.name()).apply();
    }

    public static ListViewMode getViewMode(Context context) {
        return ListViewMode.valueOf(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_VIEW_MODE, ListViewMode.List.name()));
    }

    public static void setViewMode(Context context, ListViewMode mode) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(PREF_VIEW_MODE, mode.name()).apply();
    }

    public static int getSpanCount(Context context) {
        final int width = context.getResources().getDisplayMetrics().widthPixels;
        final ListViewMode mMode = getViewMode(context);
        return Math.round(width / (mMode == ListViewMode.List ? dp(context, 240) : dp(context, 120)));
    }

    public static int dp(Context context, int size) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(new int[]{R.attr.actionBarSize});
        final int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        return toolbarHeight;
    }

    public static void openSettingInfoScreen(Context context, AppInfo ino) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", ino.getAppInfo().packageName, null));
        context.startActivity(intent);
    }

    public enum ListOption {
        InstalledOnly, WithSystemApps, SystemAppsOnly, CanBeMovedApps, CantBeMovedApps, Search;
    }

    public enum ListSort {
        Alphabet, Size, Date
    }

    public enum ListViewMode {
        List, Grid
    }

}

package maestro.my.apps.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import maestro.my.apps.AppInfo;
import maestro.my.apps.R;
import maestro.my.apps.Utils;

/**
 * Created by Artyom on 20.01.2016.
 */
public class AppSearchFragment extends AppListFragment {

    public static final String TAG = AppSearchFragment.class.getSimpleName();

    private EditText mEditText;
    private String mQuery;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText = (EditText) view.findViewById(android.R.id.edit);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mQuery = s.toString().trim();
                loadData(true);
            }
        });
        view.findViewById(android.R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public int getLayoutResource() {
        return R.layout.app_search_view;
    }

    @Override
    public AsyncTaskLoader<List<AppInfo>> getLoader() {
        return new AppsSearchLoader(getContext(), mQuery);
    }

    @Override
    public int getLoaderId() {
        return 3000;
    }

    public static final class AppsSearchLoader extends AsyncTaskLoader<List<AppInfo>> {

        private String mQuery;

        public AppsSearchLoader(Context context, String query) {
            super(context);
            mQuery = query;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public List<AppInfo> loadInBackground() {
            return TextUtils.isEmpty(mQuery) ? null : Utils.searchApps(getContext(), mQuery);
        }
    }

}

package maestro.my.apps.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import maestro.my.apps.RunningTask;
import maestro.my.apps.Utils;
import maestro.my.apps.adapters.BaseUpdateAdapter;
import maestro.my.apps.adapters.RunningAdapter;

/**
 * Created by Artyom on 04.02.2016.
 */
public class RecentListFragment extends BaseListFragment<RunningTask> {

    @Override
    public BaseUpdateAdapter getAdapter() {
        return new RunningAdapter(getContext());
    }

    @Override
    public AsyncTaskLoader<List<RunningTask>> getLoader() {
        return new RunningLoader(getContext());
    }

    @Override
    public int getLoaderId() {
        return 3000;
    }

    public static class RunningLoader extends AsyncTaskLoader<List<RunningTask>> {

        public RunningLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public List<RunningTask> loadInBackground() {
            return Utils.getRunningTasks(getContext());
        }
    }
}

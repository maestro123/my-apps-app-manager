package maestro.my.apps.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import maestro.my.apps.R;
import maestro.my.apps.SpaceItemDecoration;
import maestro.my.apps.Utils;
import maestro.my.apps.adapters.BaseUpdateAdapter;

/**
 * Created by Artyom on 20.01.2016.
 */
public abstract class BaseListFragment<T> extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener,
        LoaderManager.LoaderCallbacks<List<T>> {

    private RecyclerView mList;
    private BaseUpdateAdapter mAdapter;
    private ProgressBar mProgress;
    private View mEmpty;
    private RecyclerView.LayoutManager mLayoutManager;
    private SpaceItemDecoration mDecoration;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        PreferenceManager.getDefaultSharedPreferences(getContext()).unregisterOnSharedPreferenceChangeListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(getLayoutResource(), null);
        mList = (RecyclerView) v.findViewById(android.R.id.list);
        mList.setHasFixedSize(true);

        mProgress = (ProgressBar) v.findViewById(android.R.id.progress);
        mEmpty = v.findViewById(android.R.id.empty);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = getAdapter());
        loadData(true);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Utils.PREF_VIEW_MODE)) {
            applyListView();
        } else if (key.equals(Utils.PREF_SORT)) {
            loadData(true);
        }
    }

    @Override
    public Loader<List<T>> onCreateLoader(int id, Bundle args) {
        mAdapter.update(null);
        applyListView();
        mEmpty.animate().alpha(0).start();
        mProgress.animate().alpha(1f).start();
        return getLoader();
    }

    @Override
    public void onLoadFinished(Loader<List<T>> loader, List<T> data) {
        mAdapter.update(data);
        mProgress.animate().alpha(0).start();
        if (mAdapter.getItemCount() == 0) {
            mEmpty.animate().alpha(1f).start();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<T>> loader) {

    }

    public int getLayoutResource() {
        return R.layout.app_list_view;
    }

    public void loadData(boolean reload) {
        if (reload) {
            getLoaderManager().restartLoader(getLoaderId(), null, this);
        } else if (getLoaderManager().getLoader(getLoaderId()) == null) {
            getLoaderManager().initLoader(getLoaderId(), null, this);
        }
    }

    private void applyListView() {
        final Utils.ListViewMode mode = Utils.getViewMode(getContext());
        final int spanCount = Utils.getSpanCount(getContext());
        mList.removeItemDecoration(mDecoration);
        if (mode == Utils.ListViewMode.Grid) {
            mList.addItemDecoration(mDecoration = new SpaceItemDecoration(spanCount, Utils.dp(getContext(), 8)));
            mList.setLayoutManager(new GridLayoutManager(getContext(), spanCount));
        } else {
            mList.setLayoutManager(mLayoutManager = new GridLayoutManager(getContext(), spanCount) {
                private int extraSpace = getResources().getDisplayMetrics().heightPixels;

                @Override
                protected int getExtraLayoutSpace(RecyclerView.State state) {
                    return extraSpace;
                }
            });
        }
        mAdapter.setListMode(mode);
    }

    public abstract BaseUpdateAdapter getAdapter();

    public abstract AsyncTaskLoader<List<T>> getLoader();

    public abstract int getLoaderId();

}

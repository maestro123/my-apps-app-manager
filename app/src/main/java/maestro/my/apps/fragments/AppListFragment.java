package maestro.my.apps.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import maestro.my.apps.AppInfo;
import maestro.my.apps.Utils;
import maestro.my.apps.adapters.AppListAdapter;
import maestro.my.apps.adapters.BaseUpdateAdapter;

/**
 * Created by Artyom on 17.01.2016.
 */
public class AppListFragment extends BaseListFragment<AppInfo> {

    private static final String ARG_OPTION = "option";

    private Utils.ListOption mListOption;

    public static AppListFragment makeInstance(Utils.ListOption option) {
        AppListFragment fragment = new AppListFragment();
        Bundle args = new Bundle(1);
        args.putSerializable(ARG_OPTION, option);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mListOption = (Utils.ListOption) getArguments().getSerializable(ARG_OPTION);
        }
    }

    @Override
    public AsyncTaskLoader<List<AppInfo>> getLoader() {
        return new AppInfosLoader(getContext(), mListOption);
    }

    @Override
    public int getLoaderId() {
        return mListOption.ordinal() * 1000;
    }

    @Override
    public BaseUpdateAdapter getAdapter() {
        return new AppListAdapter(getContext());
    }

    public static class AppInfosLoader extends AsyncTaskLoader<List<AppInfo>> {

        private Utils.ListOption mOption;

        public AppInfosLoader(Context context, Utils.ListOption option) {
            super(context);
            mOption = option;
        }

        @Override
        public List<AppInfo> loadInBackground() {
            return Utils.getApps(getContext(), mOption, Utils.getSort(getContext()));
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

    }

}

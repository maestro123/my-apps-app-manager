package maestro.my.apps;

import java.util.ArrayList;

/**
 * Created by Artyom on 07.02.2016.
 */
public class AppTracker {

    private static volatile AppTracker sInstance;

    public AppTracker getInstance() {
        if (sInstance == null) {
            synchronized (AppTracker.class) {
                sInstance = new AppTracker();
            }
        }
        return sInstance;
    }

    private ArrayList<OnAppStateChangeListener> mListeners = new ArrayList<>();

    public void addOnAppStateChangeListener(OnAppStateChangeListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void removeOnAppStateChangeLisener(OnAppStateChangeListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    public interface OnAppStateChangeListener {

        public void onAppStateChanged(AppInfo info);

    }

}

package maestro.my.apps;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import io.karim.MaterialTabs;
import maestro.my.apps.fragments.AppListFragment;
import maestro.my.apps.fragments.AppSearchFragment;
import maestro.my.apps.fragments.RecentListFragment;

public class Main extends AppCompatActivity {

    private ViewPager mPager;
    private MaterialTabs mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .setCustomAnimations(R.anim.alpha_in, R.anim.alpha_out, R.anim.alpha_in, R.anim.alpha_out)
                        .replace(R.id.search_fragment_frame, new AppSearchFragment(), AppSearchFragment.TAG)
                        .commit();
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(5);
        mPager.setAdapter(new PagerNavigationAdapter(getSupportFragmentManager()));

        mTabs = (MaterialTabs) findViewById(R.id.material_tabs);
        mTabs.setViewPager(mPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_alphabet:
                Utils.setSort(this, Utils.ListSort.Alphabet);
                return true;
            case R.id.sort_size:
                Utils.setSort(this, Utils.ListSort.Size);
                return true;
            case R.id.sort_date:
                Utils.setSort(this, Utils.ListSort.Date);
                return true;
            case R.id.view_mode:
                Utils.setViewMode(this,
                        Utils.getViewMode(this) == Utils.ListViewMode.List
                                ? Utils.ListViewMode.Grid : Utils.ListViewMode.List);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class PagerNavigationAdapter extends FragmentStatePagerAdapter {

        public PagerNavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return AppListFragment.makeInstance(Utils.ListOption.SystemAppsOnly);
                case 2:
                    return new RecentListFragment();
            }
            return AppListFragment.makeInstance(Utils.ListOption.InstalledOnly);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 1:
                    return getString(R.string.system);
                case 2:
                    return getString(R.string.running);
            }
            return getString(R.string.installed);
        }
    }

}

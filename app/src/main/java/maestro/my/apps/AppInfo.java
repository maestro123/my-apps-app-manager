package maestro.my.apps;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import java.io.File;

/**
 * Created by Artyom on 17.01.2016.
 */
public class AppInfo {

    private ApplicationInfo mInfo;
    private PackageInfo mPackageInfo;
    private String mTitle;
    private String mDateFormatted;
    private String mSizeFormatted;
    private long mSize;
    private long mDate;

    public AppInfo(Context context, ApplicationInfo info, PackageInfo packageInfo) {
        mInfo = info;
        mPackageInfo = packageInfo;
        mTitle = String.valueOf(info.loadLabel(context.getPackageManager()));
        mSize = new File(info.sourceDir).length();
        mDate = packageInfo.firstInstallTime;
        mDateFormatted = Utils.formatDate(mDate);
        mSizeFormatted = Utils.formatSize(mSize);
    }

    public String getTitle() {
        return mTitle;
    }

    public ApplicationInfo getAppInfo() {
        return mInfo;
    }

    public PackageInfo getPackageInfo() {
        return mPackageInfo;
    }

    public long getDate() {
        return mDate;
    }

    public String getDateFormatted() {
        return mDateFormatted;
    }

    public long getSize() {
        return mSize;
    }

    public String getSizeFormatted() {
        return mSizeFormatted;
    }
}

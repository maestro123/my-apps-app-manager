package maestro.my.apps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import maestro.my.apps.RunningTask;
import maestro.my.apps.Utils;

/**
 * Created by Artyom on 04.02.2016.
 */
public class RunningAdapter extends BaseAppAdapter<RunningTask> {

    private SpannableStringBuilder mBuilder = new SpannableStringBuilder();

    public RunningAdapter(Context context) {
        super(context);
    }

    @Override
    public void onBindView(Holder holder, RunningTask info) {
        getPicasso().load(info.getAppInfo().packageName).into(holder.Icon);
        holder.Title.setText(info.getTitle());
        mBuilder.clear();
        mBuilder.append(info.getSizeFormatted());
        int index = mBuilder.length();
        if (getListMode() == Utils.ListViewMode.Grid) {
            mBuilder.append("\n").append(info.getDateFormatted());
        } else {
            mBuilder.append(' ').append("{").append(info.getDateFormatted()).append("}");
        }
        mBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#99000000")), index, mBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.Additional.setText(mBuilder);
    }
}

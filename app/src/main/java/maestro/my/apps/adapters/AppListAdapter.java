package maestro.my.apps.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import maestro.my.apps.AppInfo;
import maestro.my.apps.Utils;

/**
 * Created by Artyom on 17.01.2016.
 */
public class AppListAdapter extends BaseAppAdapter<AppInfo> {

    private SpannableStringBuilder mBuilder = new SpannableStringBuilder();

    public AppListAdapter(Context context) {
        super(context);
    }

    @Override
    public void onBindView(BaseAppAdapter<AppInfo>.Holder holder, AppInfo info) {
        getPicasso().load(info.getAppInfo().packageName).into(holder.Icon);
        holder.Title.setText(info.getTitle());
        mBuilder.clear();
        mBuilder.append(info.getSizeFormatted());
        int index = mBuilder.length();
        if (getListMode() == Utils.ListViewMode.Grid) {
            mBuilder.append("\n").append(info.getDateFormatted());
        } else {
            mBuilder.append(' ').append("{").append(info.getDateFormatted()).append("}");
        }
        mBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#99000000")), index, mBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.Additional.setText(mBuilder);
    }

}

package maestro.my.apps.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import maestro.my.apps.Utils;

/**
 * Created by artyom on 9/11/14.
 */
public abstract class BaseUpdateAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> { //implements View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = BaseUpdateAdapter.class.getSimpleName();

    public static final int ITEM_TYPE_ROW = 1;
    public static final int ITEM_TYPE_GRID = 2;

    private List<T> mItems = null;
    private List<T> mSelectedObjects = new ArrayList<>();
    private Context mContext;
    private LayoutInflater mInflater;
    private OnItemClickActionListener mItemClickListener;
    private Utils.ListViewMode mListMode;

    public BaseUpdateAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Context getContext() {
        return mContext;
    }

    public Resources getResources() {
        return mContext.getResources();
    }

    public LayoutInflater getLayoutInflater() {
        return mInflater;
    }

    public void setListMode(Utils.ListViewMode mode) {
        mListMode = mode;
    }

    public Utils.ListViewMode getListMode() {
        return mListMode;
    }

    public void update(List<T> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return mListMode == Utils.ListViewMode.List ? ITEM_TYPE_ROW : ITEM_TYPE_GRID;
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    public interface OnItemClickActionListener<T> {
        public void onItemClick(T item);

        public boolean onItemLongClick(T item);
    }

}
package maestro.my.apps.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;

import java.io.IOException;

import maestro.my.apps.AppInfo;
import maestro.my.apps.R;
import maestro.my.apps.Utils;

/**
 * Created by Artyom on 17.01.2016.
 */
public abstract class BaseAppAdapter<T> extends BaseUpdateAdapter<T, RecyclerView.ViewHolder> {

    private Picasso picasso;

    public BaseAppAdapter(Context context) {
        super(context);
        picasso = new Picasso.Builder(context).addRequestHandler(new RequestHandler() {
            @Override
            public boolean canHandleRequest(Request data) {
                return true;
            }

            @Override
            public Result load(Request request, int networkPolicy) throws IOException {
                try {
                    ApplicationInfo info = getContext().getPackageManager().getApplicationInfo(request.uri.toString(), PackageManager.GET_META_DATA);
                    return new Result(((BitmapDrawable) info.loadIcon(getContext().getPackageManager())).getBitmap(), Picasso.LoadedFrom.DISK);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).build();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Holder holder = new Holder(getLayoutInflater().inflate(getListMode() == Utils.ListViewMode.List
                ? R.layout.app_list_item_row : R.layout.app_list_item_grid, null));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Utils.openSettingInfoScreen(getContext(), getItem(holder.getAdapterPosition()));
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindView((Holder) holder, getItem(position));
    }

    public Picasso getPicasso(){
        return picasso;
    }

    public abstract void onBindView(Holder holder, T item);

    public class Holder extends RecyclerView.ViewHolder {

        public ImageView Icon;
        public TextView Title;
        public TextView Additional;

        public Holder(View itemView) {
            super(itemView);
            Icon = (ImageView) itemView.findViewById(android.R.id.icon);
            Title = (TextView) itemView.findViewById(android.R.id.text1);
            Additional = (TextView) itemView.findViewById(android.R.id.text2);
        }
    }

}

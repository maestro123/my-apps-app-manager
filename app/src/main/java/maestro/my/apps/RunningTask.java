package maestro.my.apps;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import java.util.Arrays;

/**
 * Created by Artyom on 06.02.2016.
 */
public class RunningTask extends AppInfo {

    private ActivityManager.RunningServiceInfo[] mServiceInfos;

    public RunningTask(Context context, ApplicationInfo info, PackageInfo packageInfo) {
        super(context, info, packageInfo);
    }

    public void addServiceInfo(ActivityManager.RunningServiceInfo info) {
        if (mServiceInfos == null) {
            mServiceInfos = new ActivityManager.RunningServiceInfo[1];
            mServiceInfos[0] = info;
        } else {
            mServiceInfos = Arrays.copyOf(mServiceInfos, mServiceInfos.length + 1);
            mServiceInfos[mServiceInfos.length - 1] = info;
        }
    }

}
